<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [ 'empresa','sector','empleados','taza','estado' ];


    public function estado(){
    	return $this->hasOne('App\Estado');
    }
}
