<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected fillable = ['estado'];

    public function empresa(){
    	return $this->belongsTo('App\Empresa');
    }
}
